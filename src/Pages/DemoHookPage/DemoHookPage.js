import React, { useState } from "react";
import ItemNum from "./ItemNum";

// KHÔNG dùng THIS
export default function DemoHookPage() {
  // state
  let [number, setNumber] = useState(100);
  //   setNumber ~ this.setState
  let handleIncrease = () => setNumber(number + 1);
  let handleDecrease = () => setNumber(number - 1);

  return (
    <div className="text-center">
      <h2>DemoHookPage</h2>
      <div>
        <button className="btn btn-success" onClick={handleIncrease}>
          +
        </button>
        <strong>{number}</strong>
        <button className="btn btn-danger" onClick={handleDecrease}>
          -
        </button>
      </div>
      <ItemNum data={number} />
    </div>
  );
}
// let colors = ["red", "blue"];

// let [c1, c2] = colors;
