import React from "react";

export default function ItemNum(props) {
  console.log(`  🚀: ItemNum -> props`, props);
  let { data } = props;
  return (
    <div>
      <h2>ItemNum</h2>
      <p className="display-2 text-warning">{data}</p>
    </div>
  );
}
