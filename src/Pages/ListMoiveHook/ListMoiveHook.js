import React, { useEffect, useState } from "react";
import ItemMovie from "./ItemMovie";
import axios from "axios";
import DetailMovie from "./DetailMovie";

export default function ListMoiveHook() {
  const [moiveArr, setMoiveArr] = useState([]);
  const [detail, setDetail] = useState({});
  let renderMovieList = () => {
    return moiveArr.map((item) => {
      return (
        <ItemMovie
          handleChangeDetail={handleChangeDetail}
          key={item.maPhim}
          movie={item}
        />
      );
    });
  };
  let handleFetchMovie = () => {
    axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01",
      method: "GET",
      headers: {
        TokenCybersoft:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MSIsIkhldEhhblN0cmluZyI6IjEyLzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDQ3NjgwMDAwMCIsIm5iZiI6MTY2NTI0ODQwMCwiZXhwIjoxNjk0NjI0NDAwfQ.SUELcPShU58ZkNS3CbFDhM02KMzll9j00ndjVSaiJ8Q",
      },
    })
      .then((res) => {
        console.log(res);
        setMoiveArr(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  let handleChangeDetail = (movie) => {
    setDetail(movie);
  };
  useEffect(() => {
    handleFetchMovie();
  }, []);

  //   card antd
  return (
    <div>
      <h2>ListMoiveHook</h2>
      <DetailMovie detail={detail} />
      {/* <button onClick={handleFetchMovie} className="btn btn-warning">
        Show list movie
      </button> */}
      <div className="row">{renderMovieList()}</div>
    </div>
  );
}
