import React from "react";
import { Card, Button } from "antd";

const { Meta } = Card;
export default function ItemMovie({ movie, handleChangeDetail }) {
  return (
    <Card
      className="col-2"
      hoverable
      style={{
        width: "100%",
      }}
      cover={
        <img
          style={{
            width: "100%",
            height: "200px",
            objectFit: "cover",
          }}
          alt="example"
          src={movie.hinhAnh}
        />
      }
    >
      <Button
        onClick={() => {
          handleChangeDetail(movie);
        }}
        type="primary"
      >
        Xem chi tiết
      </Button>
      <Meta title={movie.tenPhim} description="www.instagram.com" />
    </Card>
  );
}
