import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { PLAY_GAME, TAI } from "./redux/contant/gameConstant";
export default function KetQua() {
  let { luaChon, soBanThang, soLuotChoi } = useSelector((state) => {
    return state.gameReducer;
  });
  let dispatch = useDispatch();
  let hanlePlayGame = () => {
    dispatch({
      type: PLAY_GAME,
    });
  };

  return (
    <div className="text-center">
      <button
        onClick={hanlePlayGame}
        className="btn btn-warning px-5 py-2 my-5"
      >
        Play Game
      </button>
      <h2
        className={`display-4 ${
          luaChon == TAI ? "text-danger " : "text-dark"
        } `}
      >
        {luaChon}
      </h2>
      <h2>Số bàn thắng: {soBanThang}</h2>
      <h2>Số lượt chơi: {soLuotChoi}</h2>
    </div>
  );
}
