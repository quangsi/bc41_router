import { CHOOSE_OPTION, PLAY_GAME, TAI, XIU } from "../contant/gameConstant";
import { randomIntFromInterval } from "../../utils";

const initialState = {
  mangXucXac: [
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
  ],
  luaChon: null,
  soBanThang: 0,
  soLuotChoi: 0,
};

let gameReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case PLAY_GAME: {
      let newMangXucXac = [];
      let newSoLuotChoi = state.soLuotChoi + 1;
      let newSoBanThang = state.soBanThang;
      let tongDiem = 0;
      for (let i = 0; i < 3; i++) {
        let randomNum = randomIntFromInterval(1, 6);
        tongDiem += randomNum;
        // tạo mới 1 xuc xac
        let newXucXac = {
          img: `./imgXucSac/${randomNum}.png`,
          giaTri: randomNum,
        };
        tongDiem >= 11 && state.luaChon == TAI && newSoBanThang++;
        tongDiem < 11 && state.luaChon == XIU && newSoBanThang++;
        newMangXucXac.push(newXucXac);
      }
      return {
        ...state,
        mangXucXac: newMangXucXac,
        soLuotChoi: newSoLuotChoi,
        soBanThang: newSoBanThang,
      };
    }
    case CHOOSE_OPTION: {
      return { ...state, luaChon: payload };
    }

    default:
      return state;
  }
};
export default gameReducer;
