import React from "react";
import bgGame from "../../assets/bgGame.png";
import "./game.css";
import XucXac from "./XucXac";
import KetQua from "./KetQua";
export default function TaiXiuPage() {
  return (
    <div
      className="game_container"
      style={{ backgroundImage: `url(${bgGame})` }}
    >
      <XucXac />
      <KetQua />
    </div>
  );
}

// >= 11 : tài
