import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { CHOOSE_OPTION, TAI, XIU } from "./redux/contant/gameConstant";
export default function XucXac() {
  let { mangXucXac } = useSelector((state) => {
    return state.gameReducer;
  });
  let dispatch = useDispatch();
  let renderMangXucXac = () => {
    return mangXucXac.map((item, index) => {
      return <img src={item.img} key={index} />;
    });
  };
  let handleChooseOption = (luaChon) => {
    dispatch({
      type: CHOOSE_OPTION,
      payload: luaChon,
    });
  };

  return (
    <div className="d-flex container justify-content-between align-items-center pt-5">
      <button
        onClick={() => {
          handleChooseOption(TAI);
        }}
        className="btn btn-danger p-5"
      >
        Tài
      </button>
      <div>{renderMangXucXac()}</div>
      <button
        onClick={() => {
          handleChooseOption(XIU);
        }}
        className="btn btn-dark p-5"
      >
        Xỉu
      </button>
    </div>
  );
}
