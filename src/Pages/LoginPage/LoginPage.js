import axios from "axios";
import React, { Component } from "react";
import { message } from "antd";
export default class LoginPage extends Component {
  state = {
    username: "",
    password: "",
  };
  handleOnChangeForm = (e) => {
    let { value, name } = e.target;
    console.log(`   name`, name);
    this.setState({ [name]: value });
  };
  handleLogin = (e) => {
    e.preventDefault();
    console.log("yes login");
    axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap",
      method: "POST",
      data: {
        taiKhoan: this.state.username,
        matKhau: this.state.password,
      },
      headers: {
        TokenCybersoft:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MSIsIkhldEhhblN0cmluZyI6IjEyLzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDQ3NjgwMDAwMCIsIm5iZiI6MTY2NTI0ODQwMCwiZXhwIjoxNjk0NjI0NDAwfQ.SUELcPShU58ZkNS3CbFDhM02KMzll9j00ndjVSaiJ8Q",
      },
    })
      .then((res) => {
        message.success("Đăng nhập thành công");
        // james james
        console.log(res);
      })
      .catch((err) => {
        message.error(err.response.data.content);
        console.log(err);
      });
  };

  render() {
    return (
      <div>
        <h2>LoginPage</h2>
        <form onSubmit={this.handleLogin}>
          <div className="form-group">
            <input
              type="text"
              className="form-control"
              placeholder="Tài khoản"
              onChange={this.handleOnChangeForm}
              name="username"
              value={this.state.username}
            />
          </div>
          <div className="form-group">
            <input
              type="password"
              className="form-control"
              placeholder="Mật khẩu"
              onChange={this.handleOnChangeForm}
              name="password"
              value={this.state.password}
            />
          </div>
          <button className="btn btn-success">Đăng nhập</button>
        </form>
      </div>
    );
  }
}

let cat = {
  name: "mun",
  age: 5,
};

// cat.name = "miu";

let key = "age";
cat[key] = "miu";

// antd npm
