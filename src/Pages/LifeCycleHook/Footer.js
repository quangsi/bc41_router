import React, { memo } from "react";

function Footer({ like }) {
  console.log("footer render");
  return (
    <div className="p-5 bg-warning">
      <h3>Fotoer</h3>
      <h2>Like: {like}</h2>
    </div>
  );
}
export default memo(Footer);
// memo ~ pure component
