import { Button } from "antd";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import Footer from "./Footer";

export default function LifeCycleHook() {
  let [number, setNumber] = useState(100);
  let [like, setLike] = useState(100);
  useEffect(() => {
    console.log("did mount");
    return () => {
      console.log("will unmount");
    };
  }, []);
  useEffect(() => {
    console.log("did mount like");
  }, [like]);
  //   useEffect : didMount , didUpdate, willUnmount
  //   param1: function
  //   param2: array ( dependency list)
  //   dependency list : []=>didMount, [like]=>didUpdate
  let handleIncrease = () => setNumber(number + 1);
  let handleDecrease = () => setNumber(number - 1);

  let handlePlusLike = useCallback(() => {
    return setLike(like + 1);
  }, [like]);
  //   ;
  //   useCallback => xử lý function

  let doubleLike = useMemo(() => {
    return like * 2;
  }, [like]);
  //   useMemo xử lý kết quả tính toán
  console.log("render");
  return (
    <div className="text-center">
      <h2>LifeCycleHook</h2>
      <Button onClick={handleDecrease} type="primary" danger>
        -
      </Button>
      <strong>number:{number}</strong>
      <Button onClick={handleIncrease} type="primary">
        +
      </Button>

      <br />
      <br />

      <strong>Like: {like}</strong>
      <Button onClick={handlePlusLike} type="primary">
        plus like
      </Button>

      <h2>double like :{doubleLike}</h2>
      <Footer like={like} handlePlusLike={handlePlusLike} />
    </div>
  );
}
