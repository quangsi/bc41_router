import React, { PureComponent } from "react";

export default class Header_LifeCycle extends PureComponent {
  componentDidMount() {
    console.log("Child - Did mount");
  }
  render() {
    console.log("Chid - Re-Render");
    return <div className="p-5 bg-warning">Header_LifeCycle</div>;
  }
}
