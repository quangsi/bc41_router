import React, { Component } from "react";
import Header_LifeCycle from "./Header_LifeCycle";

export default class LifeCyclePage extends Component {
  state = {
    number: 1,
  };
  componentDidMount() {
    console.log("Parent - Did mount");
    // chỉ chạy 1 lần duy nhất sau khi giao diện render lần đầu thành công
    // dùng để gọi api
    /*   */
    let count = 0;
    // this.myCounter = setInterval(() => {
    //   count++;
    //   console.log(`  count`, count);
    // }, 500);
    // clear interval js w3
  }
  handleIncrease = () => {
    this.setState({
      number: this.state.number + 1,
    });
  };
  handleDecrease = () => {
    this.setState({
      number: this.state.number - 1,
    });
  };
  shouldComponentUpdate(nextProps, nextState) {
    // return true || false
    console.log({ nextProps, nextState });
    if (nextState.number == 5) {
      return false;
    } else {
      return true;
    }
  }
  render() {
    console.log("Parent - Re-Render");
    return (
      <div className="text-center">
        <h2>LifeCyclePage</h2>
        <Header_LifeCycle />
        <div>
          <button onClick={this.handleDecrease} className="btn btn-danger">
            -
          </button>
          <strong className="mx-5">{this.state.number}</strong>
          <button onClick={this.handleIncrease} className="btn btn-success">
            +
          </button>
        </div>
      </div>
    );
  }
  componentDidUpdate(preProps, preState) {
    console.log({ preProps, preState });
    // tự động đc chạy sau khi render()
    console.log("did update ");
  }
  componentWillUnmount() {
    // clearInterval(this.myCounter);
    console.log("Parrent - will unmount ");
  }
}
// life-cycle
