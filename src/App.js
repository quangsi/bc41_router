import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import Header from "./Components/Header/Header";
import DemoHookPage from "./Pages/DemoHookPage/DemoHookPage";
import HomePage from "./Pages/HomePage/HomePage";
import LifeCycleHook from "./Pages/LifeCycleHook/LifeCycleHook";
import LifeCyclePage from "./Pages/LifeCyclePage/LifeCyclePage";
import ListMoiveHook from "./Pages/ListMoiveHook/ListMoiveHook";
import LoginPage from "./Pages/LoginPage/LoginPage";
import TaiXiuPage from "./Pages/TaiXiuPage/TaiXiuPage";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Header />

        <Routes>
          {/* nơi liệt kê các page của web */}
          {/* 1 page là 1 Route */}
          <Route path="/" element={<HomePage />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/life-cycle" element={<LifeCyclePage />} />
          <Route path="/demo-hook" element={<DemoHookPage />} />
          <Route path="/movie-list" element={<ListMoiveHook />} />
          <Route path="/life-cycle-hook" element={<LifeCycleHook />} />
          <Route path="/tai-xiu" element={<TaiXiuPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
