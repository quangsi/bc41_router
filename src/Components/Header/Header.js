import React, { Component } from "react";
import { NavLink } from "react-router-dom";

export default class Header extends Component {
  render() {
    return (
      <div>
        <button className="btn btn-primary">
          <NavLink className={"text-white"} to="/">
            Home
          </NavLink>
        </button>
        <button className="btn btn-primary mx-1">
          <NavLink className={"text-white"} to="/login">
            Login
          </NavLink>
        </button>
        <button className="btn btn-primary">
          <NavLink className={"text-white"} to="/life-cycle">
            Life Cycle
          </NavLink>
        </button>
        <button className="btn btn-primary mx-1">
          <NavLink className={"text-white"} to="/demo-hook">
            Demo Hook
          </NavLink>
        </button>
        <button className="btn btn-primary mx-1">
          <NavLink className={"text-white"} to="/movie-list">
            Movie List
          </NavLink>
        </button>
        <button className="btn btn-primary mx-1">
          <NavLink className={"text-white"} to="/life-cycle-hook">
            Life Cycle Hook
          </NavLink>
        </button>
        <button className="btn btn-primary mx-1">
          <NavLink className={"text-white"} to="/tai-xiu">
            Tài xỉu
          </NavLink>
        </button>
      </div>
    );
  }
}
